# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


class Node:
    def __init__(self, data, value):
        self.data = data
        self.child = []
        self.parent = None
        self.value = value
        self.under = [value]

    def add_child(self, child):
        child.parent = self
        child.adding(child.value)
        self.child.append(child)

    def sum(self):
        res = self.value
        for c in self.child:
            res += c.sum()
        return res

    def other_sum(self):
        res = 0
        for val in self.under:
            res += val
        return res

    def count_subtree_size(self):
        res = 1
        for c in self.child:
            res += c.count_subtree_size()
        return res

    def avg(self):
        return self.sum() / self.count_subtree_size()

    def other_avg(self):
        return self.other_sum() / len(self.under)

    def print_under(self):
        for val in self.under:
            print(val)

    def median(self):
        self.under.sort()
        if len(self.under) % 2 == 1:
            return self.under[len(self.under) // 2]
        return (self.under[len(self.under) // 2] + self.under[len(self.under) // 2 - 1]) / 2

    def adding(self, val):
        p = self.parent
        while p:
            p.under.append(val)
            p = p.parent

    def count(self):
        i = 0
        p = self.parent
        while p:
            i += 1
            p = p.parent
        return i

    def print_tree(self):
        if self.parent:
            counter = str("---" * self.count() + ">")
            print(counter + str(self.value))
        else:
            print(self.value)
        for c in self.child:
            c.print_tree()

    def print_result(self):
        print("Suma elementow poddrzewa dla wierzcholka o indeksie", self.data, "i wartosci", self.value,
              "rekurencyjnie", self.sum(), "i zliczajac juz elementy tablicy przy dodawaniu", self.other_sum())
        print("Srednia elementow poddrzewa dla wierzcholka o indeksie", self.data, "i wartosci", self.value,
              "rekurencyjnie", self.avg(), "i zliczajac juz elementy tablicy przy dodawaniu", self.other_avg())
        print("Mediana elementow poddrzewa dla wierzcholka o indeksie", self.data, "i wartosci", self.value,
              "ma wartosc", self.median())
        print("\n")


# Press the green button in the gutter to run the script.
def example():
    print("Drzewo z tresci zadania: \n")
    index0 = Node(0, 5)
    index1 = Node(1, 3)
    index2 = Node(2, 7)
    index3 = Node(3, 5)
    index4 = Node(4, 0)
    index5 = Node(5, 2)
    index6 = Node(6, 1)
    index7 = Node(7, 2)
    index8 = Node(8, 5)
    index9 = Node(9, 8)
    index0.add_child(index1)
    index0.add_child(index2)
    index1.add_child(index3)
    index1.add_child(index5)
    index2.add_child(index4)
    index2.add_child(index6)
    index4.add_child(index9)
    index9.add_child(index8)
    index4.add_child(index7)
    # index0.print_tree() #wypisane drzewo
    # index0.print_under() #wypisane elementy nalezace do poddrzewa index0
    index0.print_result()
    index1.print_result()
    index4.print_result()


def example2():
    print("Przykladowy test:\n")  # Test z jednym elementem
    index0 = Node(0, 5)
    index0.print_tree()
    index0.print_under()
    index0.print_result()


def example3():
    print("Przykladowy test ze zmiana ilosci elementow:\n")  # Test ze zmiana ilosci elementow
    index0 = Node(0, 0)
    index0.print_result()

    index1 = Node(1, 3)
    print("Kolejny element")
    index0.add_child(index1)
    index0.print_result()
    index1.print_result()

    index2 = Node(2, 6)
    print("Kolejny element")
    index1.add_child(index2)
    index0.print_result()
    index1.print_result()
    index2.print_result()

    index3 = Node(3, 1)
    print("Kolejny element")
    index2.add_child(index3)
    index0.print_result()
    index1.print_result()
    index2.print_result()
    index3.print_result()

    index4 = Node(4, 5)
    print("Kolejny element")
    index0.add_child(index4)
    index0.print_result()
    index1.print_result()
    index2.print_result()
    index3.print_result()
    index4.print_result()

if __name__ == '__main__':
    example()
    example2()
    example3()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
